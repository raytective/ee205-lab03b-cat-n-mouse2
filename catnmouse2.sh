#!/bin/bash

DEFAULT_MAX_NUMBER=2048
USER_GUESS=-10

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}

#echo The max value is $THE_MAX_VALUE

THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))

#echo The number I\'m thinking of is $THE_NUMBER_IM_THINKING_OF

while [ $USER_GUESS -ne $THE_NUMBER_IM_THINKING_OF ]
do
   echo OK cat, I\'m thinking of a number from 1 to $THE_MAX_VALUE. Make a guess: 
   read USER_GUESS

   if [ $USER_GUESS -lt 1 ]
   then
      echo You must enter a number that\'s \>\= 1
   elif [ $USER_GUESS -gt $THE_MAX_VALUE ]
   then
      echo You must enter a number that\'s \<\= $THE_MAX_VALUE.
   elif [ $USER_GUESS -gt $THE_NUMBER_IM_THINKING_OF ]
   then
      echo No cat... the number I\'m thinking of is smaller than $USER_GUESS
   elif [ $USER_GUESS -lt $THE_NUMBER_IM_THINKING_OF ]
   then
      echo No cat... the number I\'m thinking of is larger than $USER_GUESS
   else
      echo You got me!
      echo    /\\_/\\
      echo  \( o.o \)
      echo   \> ^ \<
   fi
done




